def getBackdropBox(node):
     n = node
     cx = n['xpos'].value()
     cy = n['ypos'].value()
     cw = n['bdwidth'].value()
     ch = n['bdheight'].value()
     return [cx,cy,cw,ch]

def setBackdropBox(node,x=0,y=0,w=0,h=0):
    n = node
    cx = n['xpos'].setValue(x)
    cy = n['ypos'].setValue(y)
    cw = n['bdwidth'].setValue(w)
    ch = n['bdheight'].setValue(h)
    return 1

setBackdropBox(nuke.toNode("BackdropNode1"),0,0,150,150)