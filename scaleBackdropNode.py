'''
File: scaleBackdropNode
Info: Scales backdrop(s) according to a pivot point and a slider
'''
import nuke
from PySide import QtGui, QtCore

class _MainWindow(QtGui.QDialog):
    def __init__(self):
        super(_MainWindow,self).__init__()
        # VARIABLES
        self.bdNode = None
        self.scaleMode = False # if true, scale by percentage, if not, by slider value
        self.bdBoxStart = []
        self.defaultSliderRange = [-100, 100]
        self.defaultSliderValue = 0
        self.pivots = [
            [0.0, 0.0], [0.5, 0.0], [1.0, 0.0],
            [0.0, 0.5], [0.5, 0.5], [1.0, 0.5],
            [0.0, 1.0], [0.5, 1.0], [1.0, 1.0]
        ]
        self.pivotX = self.pivots[4][0]
        self.pivotY = self.pivots[4][1]

        # GUI
        # Layout
        self.masterLayout = QtGui.QVBoxLayout()
        self.setLayout(self.masterLayout)

        # Slider
        self.slider = QtGui.QSlider()
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setRange(self.defaultSliderRange[0], self.defaultSliderRange[1])
        self.slider.setValue(self.defaultSliderValue)
        # Slider events
        self.slider.sliderPressed.connect(self.sliderStart)
        self.slider.valueChanged.connect(self.sliderGo)
        self.slider.sliderReleased.connect(self.sliderEnd)

        # Assemble
        self.masterLayout.addWidget(self.slider)

        self.resize(200,50)

    def sliderStart(self):
        '''
        Fetch initial position and size of node(s)
        '''
        self.bdNode = getBdNode() # XXX WIP
        self.bdBoxStart = getBackdropBox(self.bdNode)
        print "bdBoxStart=",self.bdBoxStart
        return 1

    def sliderGo(self):
        print "GO"
        sliderVal = self.slider.value()
        print "sliderVal=",sliderVal

        power = 1
        multiplier = 0
        multiplier = abs(sliderVal)/100.0*power+1.0
        multiplier *= 1#multiplier
        print "multiplier=",multiplier
        
        # Current backdrop box
        cx = self.bdBoxStart[0]
        cy = self.bdBoxStart[1]
        cw = self.bdBoxStart[2]
        ch = self.bdBoxStart[3]
        
        # New backdrop size
        if self.scaleMode:
            if sliderVal > 0:
                nw = cw*multiplier
                nh = ch*multiplier
            else:
                nw = cw/multiplier
                nh = ch/multiplier
        else:
            nw = cw+sliderVal
            nh = ch+sliderVal

        # Difference
        dw = nw-cw
        dh = nh-ch

        # New backdrop position
        nx = cx-(dw*self.pivotX)
        ny = cy-(dh*self.pivotY)

        # Set Backdrop box
        print "bdBoxNew=",[nx,ny,nw,nh]
        setBackdropBox(self.bdNode,nx,ny,nw,nh)

    def sliderEnd(self):
        '''
        Reset slider value to default
        '''
        print "END"
        self.slider.blockSignals(True)
        self.slider.setValue(self.defaultSliderValue)
        self.slider.blockSignals(False)

def getBdNode():
    # XXX WIP FUNCTION !!!!
    return nuke.toNode("BackdropNode1")

def getBackdropBox(node):
    '''
    Returns current position and scale of the input backdrop node
    '''
    n = node
    cx = n['xpos'].value()
    cy = n['ypos'].value()
    cw = n['bdwidth'].value()
    ch = n['bdheight'].value()

    return [cx,cy,cw,ch]

def setBackdropBox(node,x=0,y=0,w=0,h=0):
    n = node
    cx = n['xpos'].setValue(x)
    cy = n['ypos'].setValue(y)
    cw = n['bdwidth'].setValue(w)
    ch = n['bdheight'].setValue(h)
    return 1

def scaleNodesFromPivot(node, pivot=0, enableX=True, enableY=True):
    '''
    Scales backdrop(s) according to a pivot point and a slider
    '''

def scaleBackdropNode():
    mWin = _MainWindow()
    mWin.exec_()

# if __name__ == '__main__':
#     scaleBackdropNode()

'''
# FOR TESTING IN NUKE
nuke.pluginAddPath("/Users/fynn/nass/work/development/BitBucket/fynnay/Nuke/scaleBackdropNode")
import scaleBackdropNode
reload(scaleBackdropNode)
scaleBackdropNode.scaleBackdropNode()
'''
